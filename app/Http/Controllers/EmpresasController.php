<?php


namespace App\Http\Controllers;

use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\PHPMailer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Laravel\Ui\Presets\React;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class EmpresasController extends Controller
{
    public function index()
    {
        $empresas = DB::select('SELECT * FROM empresas WHERE logical_erase = 0');
        return view('empresas.index', compact('empresas'));
    }

    public function new()
    {
        return view('empresas.new');
    }

    public function create(Request $req)
    {
        $total = DB::select('SELECT SUM(counter) as suma from empresas WHERE logical_erase = 0;');
        $total = (int)$total[0] -> suma;
        $total = $total + (int)$req->input('porcentaje') / 10;
        if ($total <= 10) {
            try {
                DB::insert('INSERT INTO empresas (nombre, porcentaje, counter, logical_erase) VALUES (?, ?, ?, ?);',
                    [
                        $req->input('name'),
                        $req->input('porcentaje'),
                        (int)$req->input('porcentaje') / 10,
                        0
    
                    ]
                );
                echo'<script type="text/javascript">
                alert("Empresa creada");
                window.location.href="/empresas";
                </script>';
            } catch (\Throwable $th) {
                echo $th;
                echo'<script type="text/javascript">
                alert("Error al crear la empresa");
                window.location.href="/empresas";
                </script>';
            }
        } else {
            echo'<script type="text/javascript">
            alert("La suma de porcentajes excede el 100%");
            window.location.href="/empresas";
            </script>';
        }
    }

    public function edit($id)
    {
        $empresa = DB::select('SELECT * FROM empresas WHERE empresas.id = '.$id)[0];
        return view('empresas.edit', compact('empresa'));
    }

    public function update(Request $req)
    {
        try {
            DB::update("UPDATE empresas SET nombre = ?, porcentaje = ?  WHERE id = ?", 
                [
                    $req->input('name'),
                    $req->input('porcentaje'),
                    $req -> input('id')
                ]
            );
            echo'<script type="text/javascript">
            alert("Empresa actualizada");
            window.location.href="/empresas";
            </script>';
        } catch (\Throwable $th) {
            echo $th;
            echo'<script type="text/javascript">
            alert("Error al actualizar la empresa");
            </script>';
        }
    }

    public function delete($id)
    {
        try {
            DB::update("UPDATE empresas SET logical_erase = ?  WHERE id = ?", 
                    [
                        1,
                        $id
                    ]
                );
            echo'<script type="text/javascript">
              alert("Empresa eliminada");
              window.location.href="/empresas";
              </script>';
        } catch (\Throwable $th) {
            echo'<script type="text/javascript">
              alert("Error al eliminar la empresa");
              window.location.href="/empresas";
              </script>';
        }
    }

}