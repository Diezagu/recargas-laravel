<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        if (Auth::user()-> role == 'Administrador'){
            $recargas = DB::select('SELECT * FROM recargas WHERE (status = 0 OR status = 4)');
            $recargasTerminado = DB::select('SELECT * FROM recargas WHERE status = 1');
            $recargasError = DB::select('SELECT * FROM recargas WHERE status = 2');
        }elseif (Auth::user()-> role == 'Agente') {
            $recargas = DB::select('SELECT * FROM recargas WHERE (status = 0 OR status = 4) AND recargas.empresa2 = "'.Auth:: user()-> empresa.'"');
            $recargasTerminado = DB::select('SELECT * FROM recargas WHERE status = 1 AND vendedor_asignado = '.Auth:: user()-> id.' AND recargas.empresa2 = "'.Auth:: user()-> empresa.'"');
            $recargasError = DB::select('SELECT * FROM recargas WHERE status = 2 AND vendedor_asignado = '.Auth:: user()-> id.' AND recargas.empresa2 = "'.Auth:: user()-> empresa.'"');
        }else{
            $recargas = DB::select('SELECT * FROM recargas WHERE (status = 0 OR status = 4) AND recargas.empresa2 = "'.Auth:: user()-> empresa.'"');
            $recargasTerminado = DB::select('SELECT * FROM recargas WHERE status = 1 AND recargas.empresa2 = "'.Auth:: user()-> empresa.'"');
            $recargasError = DB::select('SELECT * FROM recargas WHERE status = 2 AND recargas.empresa2 = "'.Auth:: user()-> empresa.'"');
        }
        $checkFlujo = DB::select('SELECT * FROM flujo_recargas');
        return view('home', compact('recargas', 'recargasTerminado', 'recargasError', 'checkFlujo'));
    }

    // public function receiveRecarga(Request $req)
    // {
    //     $lote = $req['lote'];
    //     $folio = $req['folio'];
    //     $numero = $req['numero'];
    //     $monto = $req['monto'];
    //     $compania = $req['compania'];
    //     $folio_recarga = $req['folio_recarga'];
    //     $fecha_recarga = $req['fecha_recarga'];
    //     $plataforma = $req['plataforma'];
    //     $usuario = $req['usuario'];
    //     $sub = $req['sub'];

    //     try {
    //         DB::insert(
    //             'INSERT INTO recargas (lote, folio, numero, monto, compania, folio_recarga, fecha_recarga, plataforma, usuario, sub, status)
    //                  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);',
    //             [
    //                 $lote,
    //                 $folio,
    //                 $numero,
    //                 $monto,
    //                 $compania,
    //                 $folio_recarga,
    //                 $fecha_recarga,
    //                 $plataforma,
    //                 $usuario,
    //                 $sub,
    //                 0
    //             ]
    //         );
    //         return 'Se guardo la informcion correctamente';
    //     } catch (\Throwable $th) {
    //         return $th;
    //     }
    // }

    public function storeRecarga($id, $message, $status)
    {
        if ($status == 4) {
            $recargas = DB::select('UPDATE recargas SET status = '.$status.', vendedor_asignado = "'.Auth::user()->id.'" WHERE id = '.$id);
            echo'<script type="text/javascript">
              alert("Recarga asignada");
              window.location.href="/";
              </script>';
        }else{
            $recargas = DB::select('UPDATE recargas SET status = '.$status.', message = "'.$message.'" WHERE id = '.$id);
            echo'<script type="text/javascript">
              alert("Recarga actualizada");
              window.location.href="/";
              </script>';
        }

    }

    public function createUser()
    {
        return view('auth.register');
    }

}
