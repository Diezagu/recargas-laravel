<?php


namespace App\Http\Controllers;

use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\PHPMailer;
use Exception;
use Illuminate\Support\Facades\DB;
use stdClass;

class IndexController extends Controller
{
    public function index()
    {
        $recargas = DB::select('SELECT * FROM recargas WHERE status = 0 OR status = 4');
        $recargasTerminado = DB::select('SELECT * FROM recargas WHERE status = 1');
        $recargasError = DB::select('SELECT * FROM recargas WHERE status = 2');
    }

    public function receiveRecarga(Request $req)
    {
        $empresa2 = 'NINGUNA';
        $empresas = DB::select('SELECT * FROM `empresas` ORDER BY `empresas`.`porcentaje` ASC');
        foreach ($empresas as $empresa) {
            if ($empresa -> counter > 0) {
                $empresa2 = $empresa -> nombre;
                DB::update('UPDATE empresas SET counter = (counter - 1) WHERE id = '. $empresa -> id);
                break;
            }
        }
        if ((end($empresas) -> id == $empresa -> id) and $empresa -> counter <= 1) {
            DB::update('UPDATE empresas SET counter = ( porcentaje / 10)');
        }
        date_default_timezone_set("America/Mexico_City");
        $date = date("Y-m-d H:i:s");
        $lote = $req['lote'];
        $folio = $req['folio'];
        $numero = $req['numero'];
        $monto = $req['monto'];
        $compania = $req['compania'];
        $empresa = $req['empresa'];
        $nip = $req['nip'];
        $cuenta = $req['cuenta'];
        $gps = $req['gps'];
        $folio_recarga = $req['folio_recarga'];
        $fecha_recarga = $req['fecha_recarga'];
        $plataforma = $req['plataforma'];
        $usuario = $req['usuario'];
        $sub = $req['sub'];

        try {
            DB::insert(
                'INSERT INTO recargas (lote, folio, numero, monto, compania, folio_recarga, fecha_recarga, plataforma, empresa, empresa2, nip, cuenta, gps, usuario, sub, status, vendedor_asignado)
                     VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);',
                [
                    $lote,
                    $folio,
                    $numero,
                    $monto,
                    $compania,
                    '',
                    $date,
                    $plataforma,
                    $empresa,
                    $empresa2,
                    $nip,
                    $cuenta,
                    $gps,
                    $usuario,
                    $sub,
                    0,
                    0
                ]
            );
            return 'Se guardo la informcion correctamente';
        } catch (\Throwable $th) {
            return $th;
        }
    }

    public function storeRecarga($id, $message, $status)
    {
        date_default_timezone_set("America/Mexico_City");
        $date = date("Y-m-d H:i:s");
        if ($status == 4) {
            $recargas = DB::update('UPDATE recargas SET status = '.$status.', vendedor_asignado = "'.Auth::user()->id.'" WHERE id = '.$id);
            echo'<script type="text/javascript">
              alert("Vendedor asignado");
              window.location.href="/";
              </script>';
        }elseif ($status == 1){
            $recargaActivar = DB::select('SELECT * FROM recargas WHERE id ='.$id);

            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://9jok3v9vp2.execute-api.us-east-2.amazonaws.com/prod',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "numero1": "'.$recargaActivar[0] -> numero.'",
                "nip" : "'.$recargaActivar[0] -> nip.'",
                "fore": "'.$message.'",
                "pla":"'.$recargaActivar[0] -> plataforma.'",
                "us":"'.$recargaActivar[0] -> usuario.'",
                "emp":"'.$recargaActivar[0] -> empresa.'",
                "gps":"'.$recargaActivar[0] -> gps.'",
                "cta":"'.$recargaActivar[0] -> cuenta.'",
                "std": 1
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            echo $response;

            $respuesta = $this -> recargasAgendadas($recargaActivar[0] -> numero);
            if ($respuesta -> errors == []) {
                DB::select('UPDATE recargas SET status = '.$status.', folio_recarga = "'.$message.'", fecha_recarga = "'.$date.'" WHERE id = '.$id);
                echo'<script type="text/javascript">
                  alert("Datos almacenados");
                  window.location.href="/";
                  </script>';
            }else{
                echo'<script type="text/javascript">
                  alert("Error al agendar la recarga: '.$respuesta -> message.'");
                  window.location.href="/";
                  </script>';
            }
        }elseif ($status == 2){
            $recargas = DB::select('UPDATE recargas SET status = '.$status.', folio_recarga = "'.$message.'", fecha_recarga = "'.$date.'" WHERE id = '.$id);
            echo'<script type="text/javascript">
              alert("Datos almacenados");
              window.location.href="/";
              </script>';
        }else{
            $recargas = DB::select('UPDATE recargas SET status = 0, vendedor_asignado = 0 WHERE id = '.$id);
            echo'<script type="text/javascript">
            alert("Usuario desasignado");
            window.location.href="/";
            </script>';
        }

    }

    // Se utiliza para agendar una recarga que tenga una fecha de activacion de mas de 85 dias
    public function recargasAgendadas($phoneNum)
    {
        date_default_timezone_set("America/Mexico_City");
        $date = date('Y-m-d');
        $fec=date("Y-m-d",strtotime($date."+ 85 days"));
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://portas.celucenter.com/refills/importApi/notRunCron',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
            "refills": [
                {
                    "telephoneNumber": "'.$phoneNum.'",
                    "amount": "10",
                    "chargeDate": "'.$fec.'"
                }
            ]
        }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response);
    }

    public function getRecargas()
    {
        if (Auth:: user()-> role == 'Administrador') {
            $recargas = DB::select('SELECT * FROM recargas WHERE (status = 0 OR status = 4)');
        }else{
            $recargas = DB::select('SELECT * FROM recargas WHERE (status = 0 OR status = 4) AND recargas.empresa2 = "'.Auth:: user()-> empresa.'"');
        }
        return $recargas;
    }

    public function getMonthNum($month)
    {
        switch ($month) {
            case 'Jan':
                return '01';
            case 'Feb':
                return '02';
            case 'Mar':
                return '03';
            case 'Apr':
                return '04';
            case 'May':
                return '05';
            case 'Jun':
                return '06';
            case 'Jul':
                return '07';
            case 'Aug':
                return '08';
            case 'Sep':
                return '09';
            case 'Oct':
                return '10';
            case 'Nov':
                return '11';
            case 'Dec':
                return '12';
        }
    }

    public function generateExcel($minDate, $maxDate)
    {
        if ( $maxDate == 'null' && $minDate == 'null' ) {
            $registros = DB::select('SELECT * FROM recargas, users WHERE (recargas.status = 1 OR recargas.status = 2) and recargas.vendedor_asignado = users.id');
        } else {
            $minDate = explode(' ', $minDate);
            $maxDate = explode(' ', $maxDate);
            $minDate = $minDate[3].'-'.$this -> getMonthNum($minDate[1]).'-'.$minDate[2];
            $maxDate = $maxDate[3].'-'.$this -> getMonthNum($maxDate[1]).'-'.$maxDate[2];
            // return 'SELECT * FROM recargas, users WHERE (status = 1 OR status = 2) and (fecha_recarga BETWEEN "'.$minDate.'" and "'.$maxDate.'") and recargas.vendedor_asignado = users.id';
            $registros = DB::select('SELECT * FROM recargas, users WHERE (status = 1 OR status = 2) and (fecha_recarga BETWEEN "'.$minDate.'" and "'.$maxDate.'") and recargas.vendedor_asignado = users.id');
        }

        $dataAsists = (object) $registros;
        if (!empty($registros[0])) {
            foreach ($dataAsists as $keys) {
                $arr[] = [
                    'FOLIO' => (string) $keys->folio,
                    'NUMERO' => (string) $keys->numero,
                    'MONTO' => (string) $keys->monto,
                    'EMPRESA' => (string) $keys->empresa,
                    'COMPAÑIA' => (string) $keys->compania,
                    'FOLIO_RECARGA' => (string) $keys->folio_recarga,
                    'FECHA_RECARGA' => (string) $keys->fecha_recarga,
                    'PLATAFORMA' => (string) $keys->plataforma,
                    'USUARIO CREADOR' => (string) $keys->name,
                    'STATUS' => (string) $keys->status == 1 ? 'ACTIVADA' : 'ERROR'
                ];
            }

            return $this->generateCSV($arr, 'FOLIO,NUMERO,MONTO,EMPRESA,COMPAÑIA,FOLIO_RECARGA,FECHA_RECARGA,PLATAFORMA,USUARIO CREADOR,STATUS', 'recargas', true);
        }else{
            echo'<script type="text/javascript">
            alert("No hay datos para generar el excel");
            window.location.href="/";
            </script>';
        }
    }

    public function generateCSV($datos, $cabeceras, $nombreArchivo, $isDirectToPage = true)
    {
        // $datos = Arreglo asociativo de los datos
        // $cabeceras = los datos a mostrar en la tabla excel separados por comas, ej: 'imei,fCreacion' (TIENEN QUE SER IGUAL A COMO ESTAN EN EL ARREGLO ASOCIATIVO)
        // $nombreArchivo = aqui pos el nombre del archivo xd, no debes poner la extension .csv
        // $isDirectToPage = Si quieres mostrar un alert directamente en una pagina para cuando no hay datos.
        if (count($datos) > 0) {
            $delimiter = ",";
            $filename = $nombreArchivo . ".csv";

            //create a file pointer
            $f = fopen('php://memory', 'w');

            //set column headers
            $fields = explode(",", $cabeceras);
            fputcsv($f, $fields, $delimiter);

            foreach ($datos as $value) {
                //acciones
                $i = 0;
                $lineData = [];

                while ($i < count($fields)) {
                    # code...
                    $lineData[$i] = $value[$fields[$i]];
                    $i++;
                }
                fputcsv($f, $lineData, $delimiter);
            }
            //move back to beginning of file
            fseek($f, 0);
            //set headers to download file rather than displayed
            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename="' . $filename . '";');

            //output all remaining data on a file pointer
            return fpassthru($f);
        } else {
            return ($isDirectToPage ? '<script>alert("No hay datos"); window.location.href = "/auth/sidebar/ventas";</script>' : 'No hay datos');
        }
    }

    public function getOldRecargas()
    {
        date_default_timezone_set("America/Mexico_City");
        $now = date('Y-m-d H:i:s');
        $time = strtotime($now);
        $time = $time - (5 * 60);
        $lessFive = date("Y-m-d H:i:s", $time);
        $registros = DB::select('SELECT * FROM recargas WHERE status = 0 and fecha_recarga <= "'.$lessFive.'"');
        foreach ($registros as $registro) {
            DB::update('UPDATE recargas SET status = 2, folio_recarga = "Caducada por tiempo" WHERE id ='.$registro -> id);
            $ch = curl_init('https://www.controcel.com/ionicbotgeneral.php');
            curl_setopt ($ch, CURLOPT_POST, 1);
            curl_setopt ($ch, CURLOPT_POSTFIELDS, "usua="."parametroext". base64_encode($registro -> usuario)."&tel="."parametroext".base64_encode($registro -> numero)."&np="."parametroext".base64_encode($registro -> nip)."&gps="."parametroext".base64_encode($registro -> gps)."&emp="."parametroext".base64_encode($registro -> empresa)."&rm="."parametroext".base64_encode('5'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT_MS, 30000);
            $data1 = curl_exec($ch);
            $data1 = str_replace("\n",'\\\\n',$data1);
            $this -> sendMessageToWhats('','','','',$data1);
            $data1 = str_replace("\\\\n",'\\n',$data1);
            $this -> sendMessageTelegram('','','','',$data1);
            $pos = strpos($data1, 'TRANSACCION EXITOSA');

            if ($pos === false) {
                $curl = curl_init();
                curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://9jok3v9vp2.execute-api.us-east-2.amazonaws.com/prod',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'{
                    "numero1": "'.$registro -> numero.'",
                    "nip" : "'.$registro -> nip.'",
                    "fore": "",
                    "pla":"'.$registro -> plataforma.'",
                    "us":"'.$registro -> usuario.'",
                    "emp":"'.$registro -> empresa.'",
                    "gps":"'.$registro -> gps.'",
                    "cta":"",
                    "std": 1,
                    "bandera": "150"
                }',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                    ),
                ));
                $response = curl_exec($curl);
                curl_close($curl);
                echo $response;
            }
            curl_close($ch);
        }

        $folios = "";
        $numeros = "";
        $montos = "";
        $carriers = "";
        $now = date('Y-m-d H:i:s');
        $time = strtotime($now);
        $time = $time - (1 * 60);
        $lessOne = date("Y-m-d H:i:s", $time);
        $registros = DB::select('SELECT * FROM recargas WHERE status = 0 and fecha_recarga <= "'.$lessOne.'"');
        echo 'SELECT * FROM recargas WHERE status = 0 and fecha_recarga <= "'.$lessOne.'"';
        print_r($registros);
        foreach ($registros as $registro) {
            $folios .= $registro -> folio.', ';
            $numeros .= $registro -> numero.', ';
            $montos .= $registro -> monto.', ';
            $carriers .= $registro -> compania.', ';
        }

        if ($folios != "") {
            $this -> sendMessageTelegram($folios, $numeros, $montos, $carriers);
            $this -> sendMessageToWhats($folios, $numeros, $montos, $carriers);
        }
    }

    public function sendMessageTelegram($folios, $numeros, $montos, $carriers, $text = '')
    {
        if ($text == '') {
            $text = 'Nueva(s) recarga(s) pendiente\\n\\n📱 ('.$folios.') 📱\\nNumero: '.$numeros.'\\nMonto: '.$montos.'\\nCarrier: '.$carriers.'\\n\\n¡superchip celucenter!';
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://hvykxnff9a.execute-api.us-east-2.amazonaws.com/sendMessageTelegram',
            CURLOPT_RETURNTRANSFER => false,
            CURLOPT_ENCODING => '',
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "token": "609720599:AAGpaYv3YZKCgpSBh_XWB7Cyf3Gmh76AlVM",

                "type": "text",

                "chatId": "-1001500051848",

                "text": "'.$text.'",

                "content":""
            }',
            CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
    }

    public function sendMessageToWhats($folios, $numeros, $montos, $carriers, $text = '')
    {
        if ($text == '') {
            $text = 'Nueva(s) recarga(s) pendiente\\\\n\\\\n📱 ('.$folios.') 📱\\\\nNumero: '.$numeros.'\\\\nMonto: '.$montos.'\\\\nCarrier: '.$carriers.'\\\\n\\\\n¡superchip celucenter!';
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://it2lz4ded0.execute-api.us-east-2.amazonaws.com/sendMessage',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
            "number":"",
            "groupid":"120363037671756340",
            "message": "'.$text.'",
            "token":"9af25bb732ee0439e642abf86c730603a0285eb9",
            "application":"10",
            "globalmedia":"",
            "groupmention":""
        }',

        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
        ));
        $response = curl_exec($curl);

        curl_close($curl);
    }

    public function setActivo($status)
    {
        try {
            DB::update('UPDATE flujo_recargas SET activo = '.$status);
        } catch (\Throwable $th) {
            echo $th;
        }
    }

    // ws para traerse las recargas pendientes
    public function getPending()
    {
        $response = new stdClass();
        try {
            $response->error = false;
            $recargas = DB::select('SELECT * FROM recargas WHERE status = 0');
            $response -> message = $recargas;
            return $response;
        } catch (Exception $th) {
            $response->error = true;
            $response -> message = $th;
            return $response;
        }
    }

    public function placeRecharge(Request $req)
    {
        $response = new stdClass();
        $numero = $req -> numero;
        $folio_recarga = $req -> folio_recarga;
        $status = $req -> status;
        $mensaje = $req -> mensaje;

        if ($status == '' || $folio_recarga == '' || $numero == '') {
            $response -> error = true;
            $response -> message = 'Invalid data';
        } else {
            try {
                if ($status == 1) {
                    DB::update("UPDATE recargas SET status = {$status} WHERE numero = '{$numero}'");
                    $response -> error = false;
                    $response -> message = "Data created successfully";
                    $response -> number = $numero;
                } elseif ($status == 2) {
                    DB::update("UPDATE recargas SET status = {$status}, message = '{$mensaje}' WHERE numero = '{$numero}'");
                    $response -> error = false;
                    $response -> message = "Data created succsessfully with incorrect value";
                    $response -> number = $numero;
                } else {
                    $response -> error = true;
                    $response -> message = "Uknown status";
                }
            } catch (Exception $th) {
                $response -> error = true;
                $response -> message = $th;
            }
        }
        return $response;
    }
}
