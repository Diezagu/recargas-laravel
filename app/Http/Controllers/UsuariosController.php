<?php


namespace App\Http\Controllers;

use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\PHPMailer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class UsuariosController extends Controller
{
    public function index()
    {
        $usuarios = DB::select('SELECT * FROM users WHERE logical_erase = 0');
        return view('usuarios.index', compact('usuarios'));
    }

    public function create()
    {
        $empresas = DB::select('SELECT * FROM empresas WHERE logical_erase = 0');
        return view('usuarios.create', compact('empresas'));
    }

    public function edit($id)
    {
        $usuario = DB::select('SELECT * FROM users WHERE users.id = '.$id)[0];
        $empresas = DB::select('SELECT * FROM empresas');
        return view('usuarios.edit', compact('usuario', 'empresas'));
    }

    public function update(Request $req)
    {   
        try {
            if ($req -> input('password') == '') {
                DB::update("UPDATE users SET name = ?, email = ?, role = ?, empresa = ?  WHERE id = ?", 
                    [
                        $req->input('name'),
                        $req->input('email'),
                        $req->input('role'),
                        $req->input('empresa'),
                        $req -> input('id')
                    ]
                );
            } else {
                $password = $req -> input('password');
                DB::update("UPDATE users SET name = ?, email = ?, role = ?, password = ?  WHERE id = ?", 
                    [
                        $req->input('name'),
                        $req->input('email'),
                        $req->input('role'),
                        Hash::make($password),
                        $req -> input('id')
                        
                    ]
                );
            }
            echo'<script type="text/javascript">
              alert("Usuario actualizado");
              window.location.href="/usuarios";
              </script>';
        } catch (\Throwable $th) {
            echo'<script type="text/javascript">
              alert("Error al actualizar usuario '.$th.'");
              window.location.href="/usuarios";
              </script>';
        }
    }

    public function delete($id)
    {
        try {
            DB::update("UPDATE users SET logical_erase = ?  WHERE id = ?", 
                    [
                        1,
                        $id
                    ]
                );
            echo'<script type="text/javascript">
              alert("Usuario eliminado");
              window.location.href="/usuarios";
              </script>';
        } catch (\Throwable $th) {
            echo'<script type="text/javascript">
              alert("Error al eliminar usuario '.$th.'");
              window.location.href="/usuarios";
              </script>';
        }
    }

    public function setStatus($id, $status)
    {
        try {
            DB::update("UPDATE users SET active = ?  WHERE id = ?", 
                [
                    $status,
                    $id
                ]
            );
            if ($status == 1) {
                echo'<script type="text/javascript">
                alert("Usuario activado");
                window.location.href="/usuarios";
                </script>';
            } else {
                echo'<script type="text/javascript">
                alert("Usuario desactivado");
                window.location.href="/usuarios";
                </script>';
            }

        } catch (\Throwable $th) {
            echo'<script type="text/javascript">
              alert("Error al cambiar status a usario: "'.$th.');
              window.location.href="/usuarios";
              </script>';
        }
    }

    public function store(Request $req)
    {
        $remember_token = str_random(50);
        date_default_timezone_set("America/Mexico_City");
        $date = date("Y-m-d H:i:s");
        $password = $req -> input('password');

        try {
            DB::insert(
                'INSERT INTO users (
                                          name,
                                          email,
                                          email_verified_at,
                                          password,
                                          role,
                                          empresa,
                                          logical_erase,
                                          active,
                                          remember_token,
                                          created_at,
                                          updated_at
                                        )
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);',
                [
                    $req->input('name'),
                    $req->input('email'),
                    null,
                    Hash::make($password),
                    $req -> input('role'),
                    $req -> input('empresa'),
                    0,
                    1,
                    $remember_token,
                    $date,
                    $date
                    
                ]
            );
            echo'<script type="text/javascript">
            alert("Usuaro creado");
            window.location.href="/usuarios";
            </script>';
        } catch (\Throwable $th) {
            return $th;
        }
    }
}