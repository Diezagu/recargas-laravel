@extends('layouts.interface')
@section('content')

<div class="d-flex bd-highlight justify-content-center" style="margin-top: 3%">
    <div class="p-2 w-50 bd-highlight">
        <div class="card">
            <div class="card-header bg-primary">
                <h3>Crear Usuario</h3>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ url('/crearUsuario') }}" style="margin-top: 4%">
                    <div class="mb-3">
                      <label for="name" class="form-label">Nombre</label>
                      <input type="text" class="form-control" name="name" id="name" required>
                    </div>

                    <div class="mb-3">
                      <label for="email" class="form-label">Correo Electroncio</label>
                      <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" required>
                    </div>

                    <div class="mb-3">
                        <label for="selectRole">Sleccionar Rol</label>
                        <select class="form-control" id="selectRole" name="role" aria-label="Default select example" required>
                            <option selected disabled></option>
                            <option value="Agente">Agente</option>
                            <option value="Supervisor">Supervisor</option>
                            <option value="Administrador">Administrador</option>
                        </select>
                    </div>

                    <div class="mb-3">
                        <label for="selectRole">Empresa</label>
                        <select class="form-control" id="selectEmpresa" name="empresa" aria-label="Default select example" required>
                            <option selected disabled></option>
                            @foreach($empresas as $empresa)
                                <option value="{{$empresa -> nombre}}">{{$empresa -> nombre}} ({{$empresa -> porcentaje}}%)</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="mb-3">
                        <label for="password" class="form-label">Contraseña</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div style="margin-top: 7%">
                        <button type="submit" class="btn btn-success">Crear Usuario</button>
                        <a class="btn btn-secondary" href="/usuarios" role="button">Regresar</a>
                    </div>
                  </form>
            </div>
        </div>
        
    </div>
</div>

@endsection