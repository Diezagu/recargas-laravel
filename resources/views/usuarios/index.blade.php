@extends('layouts.interface')

@section('content')

<div class="card" style="margin: 1%;">
    <div class="card-header bg-primary mb-1 d-flex justify-content-between">
        <h3>Usuarios</h2>
        <div style="display: flex; margin: 1%;">
            <a class="btn btn-success" href="/crearUsuario" role="button">Crear Usuario</a>
        </div>
    </div>
    <div class="card-body">
        <table id="usersTable" style="width:100%;" class="table table-striped table-bordered dt-responsive nowrap">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Rol</th>
                    <th>Empresa</th>  
                    <th></th>               
                </tr>
            </thead>
            <tbody>
                @foreach ($usuarios as $usuario)
                    <tr>
                        <td>{{$usuario -> id}}</td>
                        <td>{{$usuario -> name}}</td>
                        <td>{{$usuario -> role}}</td>
                        <td>{{$usuario -> empresa}}</td>
                        <td>
                            <a href="javascript:editUser({{$usuario -> id}})" title="Editar" style="color: #575dff; font-size: 2rem"><i class="fas fa-edit"></i></a>
                            <a href="javascript:deleteUser({{$usuario -> id}})" onclick="return confirm('¿Estas seguro?')" title="Eliminar" style="color: #ff5757; font-size: 2rem"><i class="fas fa-trash-alt"></i></a>
                            @if($usuario -> active == 1)
                                <a href="javascript:setUserStatus({{$usuario -> id}}, 0)" title="Desactivar" style="color: #000000; font-size: 2rem"><i class="fas fa-arrow-down"></i></a>
                             @else
                                <a href="javascript:setUserStatus({{$usuario -> id}}, 1)" title="Activar" style="color: #00FF00; font-size: 2rem"><i class="fas fa-arrow-up"></i></a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<script>
    $(document).ready(function() {
        var table = $('#usersTable').DataTable( {
            "columns": [
                null,
                null,
                null,
                null,
                { "width": "5%" },
            ]
        });
    });

    function editUser(id) {
        window.location.href="/editarUsuario/"+id;
    }

    function deleteUser(id) {
        window.location.href="/eliminarUsuario/"+id;
    }

    function setUserStatus(id, status) {
        window.location.href="/setStatusUsuario/"+id+"/"+status;
    }


</script>

@endsection