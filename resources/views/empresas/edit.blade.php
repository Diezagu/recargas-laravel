@extends('layouts.interface')
@section('content')

<div class="d-flex bd-highlight justify-content-center" style="margin-top: 3%">
    <div class="p-2 w-50 bd-highlight">
        <div class="card">
            <div class="card-header bg-primary">
                <h3>Editar Empresa</h3>
            </div>
            <div class="card-body">
                <form method="POST" action="/empresa" style="margin-top: 4%">
                    <div class="mb-3">
                      <label for="name" class="form-label">Nombre</label>
                      <input type="text" class="form-control" value="{{$empresa -> nombre}}" name="name" id="name" required>
                    </div>

                    <div class="mb-3">
                    <input id="id" name="id" type="text" hidden value="{{$empresa -> id}}">
                        <label for="porcentaje" class="form-label">Porcentaje</label>
                        <input type="porcentaje" class="form-control" value="{{$empresa -> porcentaje}}" name="porcentaje" id="porcentaje" aria-describedby="emailHelp" required>
                    </div>

                    <div style="margin-top: 7%">
                        <button type="submit" class="btn btn-success">Editar Empresa</button>
                        <a class="btn btn-secondary" href="/empresas" role="button">Regresar</a>
                    </div>
                  </form>
            </div>
        </div>
        
    </div>
</div>

@endsection