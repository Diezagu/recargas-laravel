@extends('layouts.interface')
@section('content')

<div class="d-flex bd-highlight justify-content-center" style="margin-top: 3%">
    <div class="p-2 w-50 bd-highlight">
        <div class="card">
            <div class="card-header bg-primary">
                <h3>Crear Empresa</h3>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ url('/empresa/create') }}" style="margin-top: 4%">
                    <div class="mb-3">
                      <label for="name" class="form-label">Nombre</label>
                      <input type="text" class="form-control" name="name" id="name" required>
                    </div>

                    <div class="mb-3">
                      <label for="email" class="form-label">Porcentaje</label>
                      <input type="number" class="form-control" name="porcentaje" id="porcentaje" placeholder="%" aria-describedby="emailHelp" required>
                    </div>

                    <div style="margin-top: 7%">
                        <button type="submit" class="btn btn-success">Crear Empresa</button>
                        <a class="btn btn-secondary" href="/usuarios" role="button">Regresar</a>
                    </div>
                </form>
            </div>
        </div>
        
    </div>
</div>

@endsection