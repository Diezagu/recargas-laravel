@extends('layouts.interface')

@section('content')

<div class="card" style="margin: 1%;">
    <div class="card-header bg-primary mb-1 d-flex justify-content-between">
        <h3>Empresas</h2>
        <div style="display: flex; margin: 1%;">
            <a class="btn btn-success" href="/empresa/new" role="button">Crear Empresa</a>
        </div>
    </div>
    <div class="card-body">
        <table id="empresasTable" style="width:100%;" class="table table-striped table-bordered dt-responsive nowrap">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Porcentaje (%)</th>
                    <th>Acciones</th>               
                </tr>
            </thead>
            <tbody>
                @foreach ($empresas as $empresa)
                    <tr>
                        <td>{{$empresa -> id}}</td>
                        <td>{{$empresa -> nombre}}</td>
                        <td>{{$empresa -> porcentaje}}%</td>
                        <td>
                            <a href="javascript:editEmpresa({{$empresa -> id}})" title="Editar" style="color: #575dff; font-size: 2rem"><i class="fas fa-edit"></i></a>
                            <a href="javascript:deleteEmpresa({{$empresa -> id}})" onclick="return confirm('¿Estas seguro?')" title="Eliminar" style="color: #ff5757; font-size: 2rem"><i class="fas fa-trash-alt"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<script>
    $(document).ready(function() {
        var table = $('#empresasTable').DataTable( {
            
        });
    });

    function editEmpresa(id) {
        window.location.href="/empresa/"+id+'/edit';
    }

    function deleteEmpresa(id) {
        window.location.href="/empresa/"+id+'/delete';
    }
</script>

@endsection