@extends('layouts.interface')

@section('content')
    <div class="container" style="margin-left: 15%;">
        <div style="display: flex; justify-content: right;" >
            <div class="col-sm">
                <div class="card" style="width: 15rem; height: 18rem; margin-left: 10px">
                    <div style="background: #f3b072">
                        <a href="#" id="pendientes" style="font-size: 9rem; margin-left: 18%; color: #FFFFFF"><i class="fas fa-pause-circle"></i></a>
                    </div>
                    <div class="card-body">
                        <p class="card-text">Pendientes</>
                    </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="card" style="width: 15rem; height: 18rem; margin-left: 10px">
                    <div style="background: #a8f1a8">
                        <a href="#" id="terminado" style="font-size: 9rem; margin-left: 18%; color: #FFFFFF"><i class="fas fa-check"></i></a>
                    </div>
                    <div class="card-body">
                        <p class="card-text">Terminado</>
                    </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="card" style="width: 15rem; height: 18rem; margin-left: 10px">
                    <div style="background: #f0a2a2">
                        <a href="#"  id="error" style="font-size: 9rem; margin-left: 28%; color: #FFFFFF"><i class="fas fa-times"></i></a>
                    </div>
                    <div class="card-body">
                        <p class="card-text">Error</>
                    </div>
                </div>
            </div>
            @if(Auth::user()-> role == 'Administrador' or Auth::user()-> role == 'Supervisor')
                <div style="margin-top: 3%; margin-left: 2%;" class="col-sm">
                    <label class="col-sm-4 control-label" for="txtid">Flujo de recargas</label>
                    <div class="col-sm-3">
                        <span class="switch">
                            <div class="btn-group  btn-group-toggle" data-toggle="buttons">
                                @foreach($checkFlujo as $status)
                                    @if ($status -> activo == 1)
                                    <label class="btn btn-default active">
                                        <input type="radio" name="supc" id="option1" value="si" autocomplete="off" checked>✔️
                                    </label>
                                    <label class="btn btn-default">
                                        <input type="radio" name="supc" id="option2" value="no" autocomplete="off">❌
                                    </label>
                                    @else
                                        <label class="btn btn-default">
                                            <input type="radio" name="supc" id="option1" value="si" autocomplete="off">✔️
                                        </label>
                                        <label class="btn btn-default active">
                                            <input type="radio" name="supc" id="option2" value="no" autocomplete="off" checked>❌
                                        </label>
                                    @endif
                                @endforeach
                            </div>
                        </span>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="card" style="margin: 10px;">
        <div class="card-header bg-primary mb-1 d-flex justify-content-between">
            <h2>Recargas</h2>
            <a href="javascript:downloadExcel()" class="btn btn-success h-25" tabindex="-1" style="margin-top: 1%" role="button" aria-disabled="true">Descargar Reporte</a>
        </div>
        <div class="card-body card bg-light mb-3">
            <div class = "d-flex justify-content-center">
                <table id="dateFilter" cellspacing="5" cellpadding="5">
                    <tbody>
                        <tr>
                            <td><input placeholder="Desde" style="margin-right: 1%;" class="form-control" type="text" id="min" name="min"></td>
                            <td><input placeholder="Hasta" class="form-control" type="text" id="max" name="max"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <table id="tableRec" style="width:100%;" class="table table-striped table-bordered dt-responsive nowrap">
                <thead>
                    <tr>
                        <th>Folio</th>
                        <th>Numero</th>
                        <th>Monto</th>
                        <th>Empresa</th>
                        <th>Compañia</th>
                        <th>Plataforma</th>
                        <th>Fecha Ingreso</th>
                        <th>Sub</th>
                        <th data-priority="100000">No. autorizacion</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($recargas as $recarga)
                        <tr>
                            <td>{{$recarga -> folio}}</td>
                            <td>{{$recarga -> numero}}</td>
                            <td>{{$recarga -> monto}}</td>
                            <td>{{$recarga -> empresa2}}</td>
                            <td>{{$recarga -> compania}}</td>
                            <td>{{$recarga -> plataforma}}</td>
                            <td>{{$recarga -> fecha_recarga}}</td>
                            <td>{{$recarga -> sub}}</td>
                            <td>
                                @if (Auth::user() -> id == $recarga -> vendedor_asignado)
                                    <input type="text" id="messageInput" name="messageInput">
                                    <a href="javascript:sendData({{$recarga -> id}}, 1)" title="Aceptar" style="color: #a8f1a8; font-size: 2rem"><i class="fas fa-check"></i></a>
                                    <a href="javascript:sendData({{$recarga -> id}}, 2)" title="Rechazar" style="color: #f0a2a2; font-size: 2rem"><i class="fas fa-times"></i></a>
                                    <a href="javascript:sendData({{$recarga -> id}}, 5)" title="Desasignar" style="color: #000000; font-size: 1.7 rem"><i class="fas fa-undo-alt"></i></a>
                                @elseif ($recarga -> vendedor_asignado == '')
                                    <a href="javascript:sendData({{$recarga -> id}}, 4)" title="Asignar" style="color: #000000; font-size: 2rem"><i class="fab fa-get-pocket"></i></a>
                                @else
                                    <input placeholder="ASIGNADO" disabled type="text" id="message">
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <table id="tableRecTer" style="width:100%;" class="table table-striped table-bordered dt-responsive nowrap">
                <thead>
                    <tr>
                        <th>Folio</th>
                        <th>Numero</th>
                        <th>Monto</th>
                        <th>Empresa</th>
                        <th>Compañia</th>
                        <th>No. Autorizacion</th>
                        <th>Fecha Recarga</th>
                        <th>Plataforma</th>
                        <th>Sub</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($recargasTerminado as $recarga)
                        <tr>
                            <td>{{$recarga -> folio}}</td>
                            <td>{{$recarga -> numero}}</td>
                            <td>{{$recarga -> monto}}</td>
                            <td>{{$recarga -> empresa2}}</td>
                            <td>{{$recarga -> compania}}</td>
                            <td>{{$recarga -> folio_recarga}}</td>
                            <td>{{$recarga -> fecha_recarga}}</td>
                            <td>{{$recarga -> plataforma}}</td>
                            <td>{{$recarga -> sub}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <table id="tableRecErr" style="width:100%;" class="table table-striped table-bordered dt-responsive nowrap">
                <thead>
                    <tr>
                        <th>Folio</th>
                        <th>Numero</th>
                        <th>Monto</th>
                        <th>Empresa</th>
                        <th>Compañia</th>
                        <th>Error</th>
                        <th>Fecha Recarga</th>
                        <th>Plataforma</th>
                        <th>Sub</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($recargasError as $recarga)
                        <tr>
                            <td>{{$recarga -> folio}}</td>
                            <td>{{$recarga -> numero}}</td>
                            <td>{{$recarga -> monto}}</td>
                            <td>{{$recarga -> empresa2}}</td>
                            <td>{{$recarga -> compania}}</td>
                            <td>{{$recarga -> folio_recarga}}</td>
                            <td>{{$recarga -> fecha_recarga}}</td>
                            <td>{{$recarga -> plataforma}}</td>
                            <td>{{$recarga -> sub}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        var minDate, maxDate;
        // Custom filtering function which will search data in column four between two values
        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                var min = minDate.val();
                var max = maxDate.val();
                var date = new Date( data[6] );

                if (
                    ( min === null && max === null ) ||
                    ( min === null && date <= max ) ||
                    ( min <= date   && max === null ) ||
                    ( min <= date   && date <= max )
                ) {
                    return true;
                }
                return false;
            }
        );

        $(document).ready(function() {
            // Create date inputs
            startInterval();
            minDate = new DateTime($('#min'), {
                format: 'MMMM Do YYYY'
            });
            maxDate = new DateTime($('#max'), {
                format: 'MMMM Do YYYY'
            });

            // DataTables initialisation
            var table = $('#tableRecErr').DataTable();
            var table1 = $('#tableRec').DataTable({
                responsive: true,
                columnDefs: [
                    { data: 'col-Folio', "width": "5%", targets: 0 },
                    { data: 'col-NoAuth', responsivePriority: 1000, targets: -1 },
                    { data: 'col-Numero', responsivePriority: 1, targets: 1 },
                    { data: 'col-Monto', responsivePriority: 1, targets: 2 },
                    { data: 'col-Empresa', responsivePriority: 1, targets: 3 },
                    { data: 'col-Compania', responsivePriority: 1, targets: 4 },
                    { data: 'col-Plataforma', "width": "5%", targets: 5 },
                    { data: 'col-Fecha_ingreso', responsivePriority: 1, targets: 6 },
                    { data: 'col-Sub', responsivePriority: 1, targets: 7 }
                ]
            });
            var table2 = $('#tableRecTer').DataTable();

            // Refilter the table
            $('#min, #max').on('change', function () {
                table.draw();
                table2.draw();
            });

            $('#dateFilter').hide();
            $('#tableRecTer').parents('div.dataTables_wrapper').first().hide();
            $('#tableRecErr').parents('div.dataTables_wrapper').first().hide();
            $('#tableRec').parents('div.dataTables_wrapper').first().show();
        } );

        $( "#terminado" ).click(function() {
            $('#dateFilter').show();
            $('#tableRecTer').parents('div.dataTables_wrapper').first().show();
            $('#tableRecErr').parents('div.dataTables_wrapper').first().hide();
            $('#tableRec').parents('div.dataTables_wrapper').first().hide();
        });

        $( "#error" ).click(function() {
            $('#dateFilter').show();
            $('#tableRecTer').parents('div.dataTables_wrapper').first().hide();
            $('#tableRecErr').parents('div.dataTables_wrapper').first().show();
            $('#tableRec').parents('div.dataTables_wrapper').first().hide();

        });

        $( "#pendientes" ).click(function() {
            $('#dateFilter').hide();
            $('#tableRecTer').parents('div.dataTables_wrapper').first().hide();
            $('#tableRecErr').parents('div.dataTables_wrapper').first().hide();
            $('#tableRec').parents('div.dataTables_wrapper').first().show();
        });

        function sendData(id, status) {
            if (status == 1) {
                let message = document.getElementById("messageInput").value;
                if (message == '') {
                    alert('Se debe agreagar el numero de autorizacion')
                }else{
                    window.location.href="/recargas/storeData/"+id+"/"+message+"/"+status;
                }
            }else if(status == 2){
                let message = document.getElementById("messageInput").value;
                if (message == '') {
                    alert('Se debe agreagar el mensaje de error')
                }else{
                    window.location.href="/recargas/storeData/"+id+"/"+message+"/"+status;
                }
            }else if (status == 4){
                message = 'Asignado';
                window.location.href="/recargas/storeData/"+id+"/"+message+"/"+status;
            } else {
                message = 'Desasignado';
                window.location.href="/recargas/storeData/"+id+"/"+message+"/"+status;
            }
        }

        function startInterval() {
            intervalo = setInterval(updateData, 10000);
        }

        function downloadExcel() {
            miDate = minDate.s.d;
            maDate = maxDate.s.d;
            window.location.href="/descargarReporte/"+miDate+'/'+maDate;
        }

        function updateData() {
            $.ajax({
                url: "/getRecargas",
                type: "GET",
                data: '',
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    console.log('entra al intervalo');
                    var t = $('#tableRec').DataTable();
                    var dataTableNodes = [];
                    t.clear();
                    $.each(data, function (index, value) {
                        let idUser = "<?php
                            use Illuminate\Support\Facades\Auth;
                            echo (Auth::user()->id) ?>";

                        if (value.vendedor_asignado == idUser) {
                            console.log('Entra al if');
                            dataTableNodes.push({
                                "col-Folio": value.folio,
                                "col-Numero": value.numero,
                                "col-Monto": value.monto,
                                "col-Empresa": value.empresa,
                                "col-Compania": value.compania,
                                "col-Plataforma": value.plataforma,
                                "col-Fecha_ingreso": value.fecha_recarga,
                                "col-Sub": value.sub,
                                "col-NoAuth": "<input type=\"text\" id=\"messageInput\" name=\"messageInput\"><a href=\"javascript:sendData("+value.id+", 1)\" title=\"Aceptar\" style=\"color: #a8f1a8; font-size: 2rem\"><i class=\"fas fa-check\"></i></a><a href=\"javascript:sendData("+value.id+", 2)\" title=\"Rechazar\" style=\"color: #f0a2a2; font-size: 2rem\"><i class=\"fas fa-times\"></i></a><a href=\"javascript:sendData("+value.id+", 5)\" title=\"Desasignar\" style=\"color: #000000; font-size: 1.7 rem\"><i class=\"fas fa-undo-alt\"></i></a>"
                            });
                        }else if(value.vendedor_asignado == ''){
                            dataTableNodes.push({
                                "col-Folio": value.folio,
                                "col-Numero": value.numero,
                                "col-Monto": value.monto,
                                "col-Empresa": value.empresa,
                                "col-Compania": value.compania,
                                "col-Plataforma": value.plataforma,
                                "col-Fecha_ingreso": value.fecha_recarga,
                                "col-Sub": value.sub,
                                "col-NoAuth": "<a href=\"javascript:sendData("+value.id+", 4)\" title=\"Asignar\" style=\"color: #000000; font-size: 2rem\"><i class=\"fab fa-get-pocket\"></i></a>"
                            });
                        }else{
                            dataTableNodes.push({
                                "col-Folio": value.folio,
                                "col-Numero": value.numero,
                                "col-Monto": value.monto,
                                "col-Empresa": value.empresa,
                                "col-Compania": value.compania,
                                "col-Plataforma": value.plataforma,
                                "col-Fecha_ingreso": value.fecha_recarga,
                                "col-Sub": value.sub,
                                "col-NoAuth": "<input placeholder=\"ASIGNADO\" disabled type=\"text\" id=\"message\">"
                            });
                        }
                    });
                    t.rows.add(dataTableNodes).draw();
                },
                error: function (e) {
                    console.log('sin datos');
                    console.log(e);
                }
            });
        }

        $( "#option1" ).click(function() {
            $.ajax({
                url: "/flujoRecargas/1",
                type: "GET",
                data: '',
                dataType: '',
                success: function (data) {
                },
                error: function (e) {
                    console.log(e);
                }
            });
            $.ajax({
                url: "https://www.controcel.com/layouts/funcionesphp.php",
                type: "POST",
                data: 'btncb=ok&supc=si&type=1',
                dataType: 'html',
                success: function (data) {
                    console.log(data);
                    alert("Se activo el flujo");
                    window.location.href="/";
                },
                error: function (e) {
                    console.log(e);
                }
            });
        });

        $( "#option2" ).click(function() {
            $.ajax({
                url: "/flujoRecargas/0",
                type: "GET",
                data: '',
                dataType: '',
                success: function (data) {
                },
                error: function (e) {
                    console.log(e);
                }
            });
            $.ajax({
                url: "https://www.controcel.com/layouts/funcionesphp.php",
                type: "POST",
                data: 'btncb=ok&supc=no&type=1',
                dataType: 'html',
                success: function (data) {
                    console.log(data);
                    alert("Se desactivo el flujo");
                    window.location.href="/";
                },
                error: function (e) {
                    console.log(e);
                }
            });
        });
    </script>
@endsection