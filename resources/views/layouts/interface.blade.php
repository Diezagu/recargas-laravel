<!doctype html>
<html>
<head>
    <title>Control Recargas</title>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <link rel="icon" type="image/png" href="{{ asset('logo-micel.jpeg') }}">
    <script src="https://kit.fontawesome.com/df7922a11f.js" crossorigin="anonymous"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.1.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdn.datatables.net/datetime/1.1.1/js/dataTables.dateTime.min.js"></script>
    <link href="https://cdn.datatables.net/datetime/1.1.1/css/dataTables.dateTime.min.css" rel="stylesheet">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ asset('logo-micel.jpeg') }}" width="40" height="20" class="d-inline-block align-top" alt="">
            Control Recargas
        </a>
        @guest
            <!-- @if (Route::has('login'))
                <li class="nav-item"></li>
            @endif -->
        @else
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                @if (Auth::user()->role == 'Administrador')
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a style="color: #FFFFFF;" class="nav-link" href="/usuarios">Usuarios</a>
                        </li>
                        <li class="nav-item">
                            <a style="color: #FFFFFF;" class="nav-link" href="/empresas">Empresas</a>
                        </li>
                    </ul>
                @endif
            </div>
            <div class="nav-item dropdown">
                <a id="navbarDropdown" style="margin-left: 70rem; color: #FFFFFF; font-size: 1.5rem" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->role }}
                    {{ Auth::user()->name }}
                </a>

                <div class="dropdown-menu dropdown-menu-right" style="margin-left: 70rem;" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>  
            </div>
        @endguest
    </nav>
    <!-- <nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                Control Recargas
            </a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                
                <ul class="navbar-nav me-auto">

                </ul>

                
                <ul class="navbar-nav ms-auto">
                    
                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item"></li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" style="margin-left: 70rem; color: #FFFFFF; font-size: 1.5rem" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->role }}
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" style="margin-left: 80rem;" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav> -->
    <div>   
        @yield('content')
    </div>
</body>
</html>