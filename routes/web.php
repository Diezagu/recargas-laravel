<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'App\Http\Controllers\HomeController@index');
Route::get('/getRecargas', 'App\Http\Controllers\IndexController@getRecargas');
Route::get('/getOldRecargas', 'App\Http\Controllers\IndexController@getOldRecargas');
Route::get('/crearUsuario', 'App\Http\Controllers\UsuariosController@create');
Route::get('/descargarReporte/{minDate}/{maxDate}', 'App\Http\Controllers\IndexController@generateExcel');
Route::post('/crearUsuario', 'App\Http\Controllers\UsuariosController@store');
Route::post('/receiveRecarga', 'App\Http\Controllers\IndexController@receiveRecarga');
Route::get('/recargas/storeData/{id}/{message}/{status}', 'App\Http\Controllers\IndexController@storeRecarga');
Route::get('/getPending', 'App\Http\Controllers\IndexController@getPending');
Route::post('/placeRecharge', 'App\Http\Controllers\IndexController@placeRecharge');
// Route::get('/login', 'App\Http\Controllers\LoginController@index');

Auth::routes();

// Rutas usuarios
Route::get('/flujoRecargas/{status}', 'App\Http\Controllers\IndexController@setActivo');
Route::get('/usuarios', 'App\Http\Controllers\UsuariosController@index')->middleware('auth');
Route::post('/crearUsuario', 'App\Http\Controllers\UsuariosController@store')->middleware('auth');
Route::get('/eliminarUsuario/{id}', 'App\Http\Controllers\UsuariosController@delete')->middleware('auth');
Route::get('/editarUsuario/{id}', 'App\Http\Controllers\UsuariosController@edit')->middleware('auth');
Route::get('/updateUser', 'App\Http\Controllers\UsuariosController@update')->middleware('auth');
Route::get('/setStatusUsuario/{id}/{status}', 'App\Http\Controllers\UsuariosController@setStatus')->middleware('auth');

// Rutas empresas
Route::get('/empresas', 'App\Http\Controllers\EmpresasController@index')->middleware('auth');
Route::get('/empresa/new', 'App\Http\Controllers\EmpresasController@new')->middleware('auth');
Route::post('/empresa/create', 'App\Http\Controllers\EmpresasController@create')->middleware('auth');
Route::get('/empresa/{id}/edit', 'App\Http\Controllers\EmpresasController@edit')->middleware('auth');
Route::post('/empresa', 'App\Http\Controllers\EmpresasController@update')->middleware('auth');
Route::get('/empresa/{id}/delete', 'App\Http\Controllers\EmpresasController@delete')->middleware('auth');

Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);